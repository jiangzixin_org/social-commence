//自行下载小程序配置，若为uniapp，将这里注释掉
/*var config = {
	// 站点id
	siteId: '{{$siteId}}',
	// api请求地址
	baseUrl: '{{$baseUrl}}',
	// 图片域名
	imgDomain: '{{$imgDomain}}',
	// H5端域名
	h5Domain: '{{$h5Domain}}',
	// 腾讯地图key
	mpKey: '{{$mpKey}}',
	apiSecurity: Boolean(parseInt('{{$apiSecurity}}')),
	publicKey: `{{$publicKey}}`,
	//客服地址
	webSocket : '',
	//本地端主动给服务器ping的时间, 0 则不开启 , 单位毫秒
	pingInterval: 1500
};*/

//H5配置
/*var config = {
	// 站点id
	siteId: '{{$siteId}}',
	// api请求地址
	baseUrl: 'https://qq.cdbendi.com',
	// 图片域名
	imgDomain: 'https://qq.cdbendi.com',
	// H5端域名
	h5Domain: 'https://qq.cdbendi.com',
	// 腾讯地图key
	mpKey: '{{$mpKey}}',
	apiSecurity: Boolean(parseInt('{{$apiSecurity}}')),
	publicKey: `{{$publicKey}}`,
	//客服地址
	webSocket : '',
	//本地端主动给服务器ping的时间, 0 则不开启 , 单位毫秒
	pingInterval: 1500
};*/

//Uniapp配置，若自行打包，开启这里的注释

var config = {
	// 站点id
	siteId: '6',
	// api请求地址
	baseUrl: 'https://qq.cdbendi.com',
	// 图片域名
	imgDomain: 'https://qq.cdbendi.com',
	// H5端域名
	h5Domain: 'https://qq.cdbendi.com',
	// 腾讯地图key
	mpKey: '',
	//客服地址
	webSocket : '',
	//本地端主动给服务器ping的时间, 0 则不开启 , 单位毫秒
	pingInterval: 1500
};

// #ifdef H5
const match = location.href.match(/\/s(\d*)\//);
if (match) {
	config.siteId = match[1];
	config.h5Domain += '/s' + match[1];
}
// #endif

// #ifdef MP-WEIXIN
if (uni.getExtConfigSync) {
	const extConfig = uni.getExtConfigSync();
	Object.assign(config, extConfig);
}
// #endif

export default config;